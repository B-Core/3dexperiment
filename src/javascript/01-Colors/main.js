module.exports = function(THREE, sceneContainer){
  var scene = new THREE.Scene();
  var aspect = window.innerWidth / window.innerHeight;
  var camera = new THREE.PerspectiveCamera( 75, aspect, 0.1, 1000 );
  var light = new THREE.DirectionalLight(0xa29584);
  var ambiantLight = new THREE.AmbientLight(0xa29584,0.2);
  var renderer = new THREE.WebGLRenderer();
  // renderer.setClearColor( 0xf5f5f5 );

  renderer.setSize( window.innerWidth, window.innerHeight );
  sceneContainer.appendChild( renderer.domElement );

  var geometry = new THREE.BoxGeometry( 1, 1, 1 );
  var material = new THREE.MeshPhongMaterial({color : 0x45f026});
  var cube = new THREE.Mesh( geometry, material );

  light.position.set(0,1,0);

  scene.add(ambiantLight);
  scene.add(light);
  scene.add( cube );
  camera.position.z = 5;

  var render = function () {
    requestAnimationFrame( render );
    cube.rotation.x += 0.01;
    cube.rotation.y += 0.01;
    renderer.render( scene, camera );
  };

  render();
};

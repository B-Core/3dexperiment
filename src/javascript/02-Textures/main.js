module.exports = function(THREE, sceneContainer){
  var OrbitControls = require("../utils/OrbitControls.js")(THREE);
  var Detector = require("../utils/Detector.js");
  var CinematicCamera = require("../utils/CinematicCamera")(THREE);
  var scene = new THREE.Scene();
  var aspect = window.innerWidth / window.innerHeight;
  var camera = new THREE.CinematicCamera( 60, aspect, 1, 10000 );
  var renderer = new THREE.WebGLRenderer();
  var loader = new THREE.TextureLoader();
  var light = new THREE.DirectionalLight(0xf1f0e0,0.8);
  var ambiantLight = new THREE.AmbientLight(0xffffff, 0.5);
  var texture = loader.load("/public/images/water_texture2341.jpg");
  var spaceTexture = loader.load("/public/images/space.jpg");
  var specularTexture = loader.load("/public/images/alpha_water_texture2341.jpg");
  var controls = new THREE.OrbitControls( camera );
  renderer.setSize( window.innerWidth, window.innerHeight );
  sceneContainer.appendChild( renderer.domElement );

  var starfield = new THREE.Group();

  function starForge() {
		var starQty = 45000;
		var geometry = new THREE.SphereGeometry(1000, 100, 50);

    var	materialOptions = {
    		size: 1.0, //I know this is the default, it's for you.  Play with it if you want.
    		opacity: 0.7
    	};

	  var starStuff = new THREE.PointsMaterial(materialOptions);

		// The wizard gaze became stern, his jaw set, he creates the cosmos with a wave of his arms

		for (var i = 0; i < starQty; i++) {

			var starVertex = new THREE.Vector3();
			starVertex.x = Math.random() * 2000 - 1000;
			starVertex.y = Math.random() * 2000 - 1000;
			starVertex.z = Math.random() * 2000 - 1000;

			geometry.vertices.push(starVertex);

		}
		var stars = new THREE.PointCloud(geometry, starStuff);
		starfield.add(stars);
	};

  if ( ! Detector.webgl ) {Detector.addGetWebGLMessage();}

  starForge();

  var geometry = new THREE.SphereGeometry( 15, 64, 64);
  var material = new THREE.MeshPhongMaterial({color:0xcfcfcf,map: texture,specularMap: specularTexture});
  var sphere = new THREE.Mesh( geometry, material );

  var skyboxGeometry = new THREE.SphereGeometry( 1000, 25, 25);
  var skyboxMaterial =  new THREE.MeshBasicMaterial({map: spaceTexture});
  var skybox = new THREE.Mesh(skyboxGeometry, skyboxMaterial);
  skybox.material.side = THREE.BackSide;

  function onWindowResize() {
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();
    renderer.setSize( window.innerWidth, window.innerHeight );
  }

  window.addEventListener( 'resize', onWindowResize, false );

  light.position.set(1,1,1);

  renderer.setClearColor(0xffffff);

  scene.add(ambiantLight);
  scene.add(light);
  scene.add( sphere );
  scene.add(skybox);
  scene.add(starfield);

  camera.position.z = 45;
  camera.position.y = 20;

  camera.focusAt(sphere.position);

  sphere.rotation.z = THREE.Math.degToRad(18);
  var render = function () {
    requestAnimationFrame( render );
    controls.update();
    sphere.rotation.y += 0.01;
    skybox.rotation.y += 0.0003;
    skybox.rotation.x += 0.0001;
    skybox.rotation.z += 0.0002;
    camera.lookAt(sphere.position);
    renderer.render( scene, camera );
  };

  render();
};

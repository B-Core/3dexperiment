module.exports = function(THREE, sceneContainer) {
  var scene = new THREE.Scene();
  var aspect = window.innerWidth / window.innerHeight;
  var camera = new THREE.PerspectiveCamera(75, aspect, 0.1, 1000);
  var renderer = new THREE.WebGLRenderer();
  var OrbitControls = require("../utils/OrbitControls.js")(THREE);
  var controls = new THREE.OrbitControls( camera );
  // renderer.setClearColor(0xf5f5f5);

  var clock = new THREE.Clock();

  var vertexShader = `
  varying vec2 vUv;
	void main() {
		vUv = uv;
		vec4 mvPosition = modelViewMatrix * vec4( position, 1.0 );
		gl_Position = projectionMatrix * mvPosition;
	}`;

  var fragmentShader = `
  uniform float time;
  uniform vec2 resolution;
  varying vec2 vUv;
  void main( void ) {
    vec2 position = -1.0 + 2.0 * vUv;
    float red = abs( sin( position.x * position.y + time / 5.0 ) );
    float green = abs( sin( position.x * position.y + time / 4.0 ) );
    float blue = abs( sin( position.x * position.y + time / 3.0 ) );
    gl_FragColor = vec4( red, green, blue, 1.0 );
  }`;

  var uniforms = {
	time: { type: "f", value: 1.0 },
	resolution: { type: "v2", value: new THREE.Vector2() }
};

  var shaderMaterial = new THREE.ShaderMaterial({
    uniforms: uniforms,
    vertexShader: vertexShader,
    fragmentShader: fragmentShader
  });

  var sphere = new THREE.Mesh(new THREE.CubeGeometry(1,1,1), shaderMaterial);

  renderer.setSize(window.innerWidth, window.innerHeight);
  sceneContainer.appendChild(renderer.domElement);


  camera.position.z = 5;

  scene.add(sphere);

  var render = function() {
    requestAnimationFrame(render);
    var delta = clock.getDelta();
    uniforms.time.value += delta * 5;
    controls.update();
    renderer.render(scene, camera);
  };

  render();
};

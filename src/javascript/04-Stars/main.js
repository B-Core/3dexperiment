module.exports = function(THREE, sceneContainer) {
  var dat = require("../utils/DAT.GUI.js");
  var scene = new THREE.Scene();
  var aspect = window.innerWidth / window.innerHeight;
  var camera = new THREE.PerspectiveCamera(75, aspect, 0.1, 10000);
  var renderer = new THREE.WebGLRenderer();
  var OrbitControls = require("../utils/OrbitControls.js")(THREE);
  var controls = new THREE.OrbitControls( camera );
  // renderer.setClearColor(0xf5f5f5);

  var clock = new THREE.Clock();

  var vertexShader = `
  varying vec2 vUv;
	void main() {
    vUv = uv;
		gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );;
	}`;

  var fragmentShader = `
  uniform float time;
  uniform vec2 resolution;
  uniform float amplitude;

  varying vec2 vUv;
  void main( void ) {
    vec3 c;
  	float l,z=time;
  	for(int i=0;i<3;i++) {
  		vec2 uv,p=gl_FragCoord.xy/resolution;
  		uv=p;
  		p-=.5;
  		p.x*=resolution.x/resolution.y;
  		z+=.07;
  		l=length(p);
  		uv+=p/l*(sin(z)+1.)*abs(sin(l*9.-z*5.));
  		c[i]=(amplitude/cameraPosition.z)/length(abs(mod(uv,.3)-2.4));
  	}
    gl_FragColor = vec4(c/l,time);
  }`;

  var uniforms = {
	time: { type: "f", value: 1.0 },
	resolution: { type: "v2", value: new THREE.Vector4(window.innerWidth, window.innerHeight, 100, -100) },
  amplitude : { type: "f", value: 10.0 }
};

  var shaderMaterial = new THREE.ShaderMaterial({
    uniforms: uniforms,
    vertexShader: vertexShader,
    fragmentShader: fragmentShader,
    clipping : false
  });

  var sphere = new THREE.Mesh(new THREE.SphereGeometry(10,10,10), shaderMaterial);

  for(let r = 0; r< 100; r++) {
    var s = sphere.clone();
    s.position.x = Math.random() * 2000 - 1000;
    s.position.y = Math.random() * 2000 - 1000;
    s.position.z = Math.random() * 2000 - 1000;
    scene.add(s);
  }


  renderer.setSize(window.innerWidth, window.innerHeight);
  sceneContainer.appendChild(renderer.domElement);


  camera.position.z = 100;

  // controls.enablePan = false;


  var gui = new dat.GUI();
  gui.add(uniforms.resolution.value, 'x', -2000, 2000);
  gui.add(uniforms.resolution.value, 'y', -2000, 2000);
  gui.add(uniforms.resolution.value, 'z', -2000, 2000);
  gui.add(uniforms.resolution.value, 'w', -2000, 2000);
  gui.add(uniforms.amplitude, 'value').min(-0.1);

  var render = function() {
    requestAnimationFrame(render);
    var delta = clock.getDelta();
    uniforms.time.value += delta * 5;

    controls.update();
    renderer.render(scene, camera);
  };

  render();
};

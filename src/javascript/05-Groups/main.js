module.exports = function(THREE, sceneContainer){
  let scene = new THREE.Scene(),
      aspect = window.innerWidth / window.innerHeight,
      camera = new THREE.PerspectiveCamera( 100, aspect, 0.8, 1000 ),
      renderer = new THREE.WebGLRenderer(),
      group = new THREE.Group(),
      light = new THREE.DirectionalLight(0xffffff, 0.8),
      ambientLight = new THREE.AmbientLight( 0x404040, 0.5 ),
      geometry = new THREE.BoxGeometry( 1, 1, 1 ),
      material = new THREE.MeshPhongMaterial(),
      cube;


  renderer.setSize( window.innerWidth, window.innerHeight );
  sceneContainer.appendChild( renderer.domElement );

  light.position.set(0,15,0);
  group.position.x = 0;

  scene.add(light);
  scene.add(ambientLight);
  scene.add( group );

  for(var x=0; x < 100; x++) {
    cube = new THREE.Mesh( geometry, material );

    if(x<50) {
      cube.position.x = 25-x;
    } else {
      cube.position.x = 25;
    }

    group.add( cube );
  }

  for(var z=0; z < 100; z++) {
    cube = new THREE.Mesh( geometry, material );

    if(z<50) {
      cube.position.z = 25-z;
    } else {
      cube.position.z = 25;
    }

    group.add( cube );
  }

  for(var y=0; y < 100; y++) {
    cube = new THREE.Mesh( geometry, material );

    if(y<50) {
      cube.position.y = 25-y;
    } else {
      cube.position.y = 25;
    }

    group.add( cube );
  }

  camera.position.z = 15;


  let render = function () {
    requestAnimationFrame( render );
    let time = Date.now() * 0.001,
        rx = Math.sin( time * 0.7 ) * 0.02,
		    ry = Math.sin( time * 0.3 ) * 0.01,
		    rz = Math.sin( time * 0.2 ) * 0.01;

    group.traverse(function( object ) {

				object.rotation.x = rx;
				object.rotation.y += ry;
				object.rotation.z += rz;

		});

    renderer.render( scene, camera );
  };

  render();
};

var raf = require("./raf.js"),
    THREE = require("./three.min.js"),
    CSSstyle = require("../style/style.scss"),
    project = "04-Stars";

/*
  fetch partials in template based on macros in the index.html
*/
var htmlPartials = document.body.innerHTML.match(/\{\{\w+\}\}/g);
htmlPartials.forEach(function(partial, index){
  var tPartial = partial.replace("{{", "").replace("}}", "");
  var requiredPartial = require("../templates/"+tPartial+".html");
  document.body.innerHTML = document.body.innerHTML.replace(partial, requiredPartial);
});

// var checkWindow = window.setInterval(function(){
  if(window.innerWidth !== 0) {
    var sceneContainer = document.getElementById("scene");

    require(`./${project}/main.js`)(THREE, sceneContainer);

    // window.clearInterval(checkWindow);
  }
// },10);
